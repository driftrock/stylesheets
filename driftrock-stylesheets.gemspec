# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'driftrock-stylesheets/version'

Gem::Specification.new do |spec|
  spec.name          = "driftrock-stylesheets"
  spec.version       = DriftrockStylesheets::VERSION
  spec.authors       = ["Chris O'Sullivan"]
  spec.email         = ["chris.osullivan@forward.co.uk"]
  spec.description   = %q{These are shared stylesheets used between different driftrock apps}
  spec.summary       = %q{These are shared stylesheets used between different driftrock apps}
  spec.homepage      = ""
  spec.license       = ""

  spec.files         = `git ls-files`.split($/)
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.3"
  spec.add_development_dependency "rake"
  spec.add_dependency "railties"
end
